import React from 'react';
import { StyleSheet, View,Text,Button ,TextInput,TouchableOpacity} from 'react-native';
import SignIn from './components/signin';
import Registeration from './components/registeration';
import MainPage from './components/main_page';
import {createStackNavigator, createAppContainer} from 'react-navigation';


// export default function App() {
//   return (
//     <View>
//         <Registeration />
//         {/* <SignIn/> */}
//         {/* <MainPage /> */}
//     </View>
//   );
// }


class SigninScreen extends React.Component{
  state = {
    email:'',
    password:''
}
handleEmail = (text)=>{
    this.setState({email:text})
}
handlePassword =(text)=>{
    this.setState({password:text})
}

login = (email,pass)=>{
    alert("Your Email is "+email+" and Password is "+pass)
}

  render(){
    return(
      <View style={{ flex:1, backgroundColor:'powderblue', alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Register Your Account?"
        onPress={() => this.props.navigation.navigate('Details')}
      />
        <Text style={styles.mainText}> HP CENTER</Text>
                <View style={styles.box}>
                <TextInput style={styles.input}
                    placeholder='Email..'
                    placeholderTextColor='#6e8b3d'
                    autoCapitalize='none'
                    onChangeText={this.handleEmail}
                    underlineColorAndroid="transparent"
                />

                <TextInput style={styles.input}
                    placeholder='Password..'
                    placeholderTextColor='#6e8b3d'
                    autoCapitalize='none'
                    onChangeText={this.handlePassword}
                    underlineColorAndroid='transparent'
                    secureTextEntry={true}
                />


                </View>
                <View style={styles.boxStyle}>
                <TouchableOpacity 
                    style={styles.submitButton}
                    onPress={()=>this.login(this.state.email,this.state.password)}
                >
                    <Text style={styles.submitButtonText} adjustsFontSizeToFit={true} >LOGIN</Text>
                </TouchableOpacity>

                </View>

     
    </View>
    )
  }
}

class RegisterScreen extends React.Component{
  state = {
    email:'',
    password:'',
    fullname:''
}
handleEmail = (text)=>{
    this.setState({email:text})
}
handlePassword =(text)=>{
    this.setState({password:text})
}
handleFullName =(text)=>{
    this.setState({fullname:text})
}

register = (fullname,email,pass)=>{
    alert("Registeration Details: "+fullname+" Email: "+email+" Keep your password:"+pass)
}
  render(){
    return(
      <View style={{ flex:1, backgroundColor:'powderblue', alignItems: 'center', justifyContent: 'center' }}>
         <Button
        title="Login To Account!"
        onPress={() => this.props.navigation.navigate('Home')}
      />
      {/* <Text style={styles.link}>Sign In Your Account?</Text> */}
      <Text style={styles.mainText}> HP CENTER</Text>

      <View style={styles.box_reg}>
      <TextInput style={styles.input_reg}
          placeholder='Full Name..'
          placeholderTextColor='#6e8b3d'
          autoCapitalize='none'
          onChangeText={this.handleFullName}
          underlineColorAndroid='transparent'
      />

      <TextInput style={styles.input_reg}
          placeholder='Email..'
          placeholderTextColor='#6e8b3d'
          autoCapitalize='none'
          onChangeText={this.handleEmail}
          underlineColorAndroid="transparent"
      />

      <TextInput style={styles.input_reg}
          placeholder='Password..'
          placeholderTextColor='#6e8b3d'
          autoCapitalize='none'
          onChangeText={this.handlePassword}
          underlineColorAndroid='transparent'
          secureTextEntry={true}
      />
      </View>

      <View style={styles.boxStyle_reg}>
      <TouchableOpacity 
          style={styles.submitButton_reg}
          onPress={()=>this.register(this.state.fullname,this.state.email,this.state.password)}
      >
          <Text style={styles.submitButtonText_reg} adjustsFontSizeToFit={true} >REGISTER</Text>
      </TouchableOpacity>
      </View>
  </View>

    )
  }
}


const AppNavigator = createStackNavigator({
  Home:SigninScreen,
  Details:RegisterScreen
},
{
  initialRouteName:"Home",
  headerMode:'none',
  navigationOptions:{
    headerVisible:false
  }
},
);

const AppContainer = createAppContainer(AppNavigator)

export default class App extends React.Component{
  render(){
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  container:{
      justifyContent:'center',
      height:700,
      alignItems:'center',
      backgroundColor:'powderblue'
  },
  box:{
      width:320,
      height:100,
      padding:10,
  },
  link:{
      color:'blue',
      fontSize:13,
  },
  boxStyle:{
      padding:60,
  },
  mainText:{
      color:'#6e8b3d',
      fontSize:24,
      textAlign:"center"
  },
  input:{
      margin:20,
      height:40,
      borderColor:'green',
      borderWidth:1,
      borderRadius:5,
      fontSize:20,
      padding:10,
//      paddingTop:18,

  },
  submitButton:{
      backgroundColor:'#6e8b3d',
      height:40,
      width:100,
      margin:16,
      borderRadius:15,
  },
  submitButtonText:{
      color:'white',
      fontSize:19,
      padding:10,
      fontWeight:'bold',
      textAlign:'center'
  },
  container_reg:{
    justifyContent:'center',
    height:700,
    alignItems:'center',
    backgroundColor:'powderblue'
},
box_reg:{
    width:320,
    height:100,
    padding:10
},
boxStyle_reg:{
    padding:140,
},

link_reg:{
    color:'blue',
    fontSize:10,
},
mainText_reg:{
    color:'#6e8b3d',
    fontSize:20,
    textAlign:"center"
},
input_reg:{
    margin:20,
    height:40,
    borderColor:'green',
    borderWidth:1,
    borderRadius:5,
    fontSize:20,
//    paddingTop:18, 
    padding:10
},
submitButton_reg:{
    backgroundColor:'#6e8b3d',
    height:40,
    width:110,
    margin:16,
    borderRadius:15,
},
submitButtonText_reg:{
    color:'white',
    fontSize:19,
    padding:10,
    fontWeight:'bold',
    textAlign:'center'
}

  
})


