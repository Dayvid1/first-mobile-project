import React from 'react'
import {Router, Scene} from 'react-native-router-flux'
import Registeration from './registeration'
import Signin from './signin'


const Routes = () =>(
    <Router>
        <Scene key = 'root'>
            <Scene key='register' component={Registeration} title='Register' />
            <Scene key='login' component={Signin} title='Login' initial={true}/>
        </Scene>
    </Router>
)

export default Routes