import React, {Component} from 'react';
import {Text, Image, View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native'


class MainPage extends Component{

    state={

    }

    render(){
        return(
            <View style={styles.container}>
                 <Text style={styles.mainText}> HP CENTER</Text>
                <ScrollView>
                    <View style={styles.box}>
                        <Image style={styles.img} source={require('../assets/first-sale.jpg')} />
                        <TouchableOpacity style={styles.inputButton}>
                           <Text style={styles.inputText}>N200,000</Text>
                        </TouchableOpacity>
                       
                    </View>
                    <View style={styles.box}>
                        <Image style={styles.img} source={require('../assets/second-sale.jpg')} />
                        <TouchableOpacity style={styles.inputButton}>
                            <Text style={styles.inputText}>N180,000</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={styles.boxStyle}>
                        <TouchableOpacity 
                        style={styles.submitButton}>
                            <Text style={styles.submitButtonText}>BUY</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default MainPage

const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        height:700,
        alignItems:'center',
        backgroundColor:'powderblue',
        padding:50,
    },
    box:{
        width:300,
        height:190,
        padding:50,
        flex:1,
        flexDirection:'row'
    },
  item:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      padding:30,
      margin:2,
      borderColor:'black',
      borderWidth:1,
      backgroundColor:'#cdc8b1'
  },
  mainText:{
    color:'#6e8b3d',
    fontSize:20,
    textAlign:"center"
  },
  boxStyle:{
    padding:60,
},
  img:{
    width:190,
    height:120,
    flex:1,
    borderRadius:10
  },
  inputButton:{
      backgroundColor:'#6e8b3d',
      borderRadius:12,
      width:80,
      height:20,
  },
  submitButton:{
    backgroundColor:'#6e8b3d',
    height:30,
    width:80,
    margin:16,
    borderRadius:15,
},
inputText:{
textAlign:'center',
fontSize:15,
color:'pink'
},
submitButtonText:{
    color:'#cdc8b1',
    fontSize:19,
    textAlign:'center'
}
})