import React, {Component} from 'react'
import {View, Text, TouchableOpacity, TextInput, StyleSheet} from 'react-native'

class SignIn extends Component{
    state = {
        email:'',
        password:'',
        fullname:''
    }
    handleEmail = (text)=>{
        this.setState({email:text})
    }
    handlePassword =(text)=>{
        this.setState({password:text})
    }
    handleFullName =(text)=>{
        this.setState({fullname:text})
    }

    register = (email,pass)=>{
        alert("Registeration Details: "+fullname+" "+email+"Keep your password:"+pass)
    }

    render(){
        return(
            <View style={styles.container}>
                {/* <Text style={styles.link}>Sign In Your Account?</Text> */}
                <Text style={styles.mainText}> HP CENTER</Text>

                <View style={styles.box}>
                <TextInput style={styles.input}
                    placeholder='FullName'
                    placeholderTextColor='#caff70'
                    autoCapitalize='none'
                    onChangeText={this.handleFullName}
                    underlineColorAndroid='transparent'
                />

                <TextInput style={styles.input}
                    placeholder='Email'
                    placeholderTextColor='#caff70'
                    autoCapitalize='none'
                    onChangeText={this.handleEmail}
                    underlineColorAndroid="transparent"
                />

                <TextInput style={styles.input}
                    placeholder='Password'
                    placeholderTextColor='#caff70'
                    autoCapitalize='none'
                    onChangeText={this.handlePassword}
                    underlineColorAndroid='transparent'
                    secureTextEntry={true}
                />
                </View>

                <View style={styles.boxStyle}>
                <TouchableOpacity 
                    style={styles.submitButton}
                    onPress={()=>this.register(this.state.fullname,this.state.email,this.state.password)}
                >
                    <Text style={styles.submitButtonText} adjustsFontSizeToFit={true} >REGISTER</Text>
                </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default SignIn;

const styles = StyleSheet.create({
    container_reg:{
        justifyContent:'center',
        height:700,
        alignItems:'center',
        backgroundColor:'powderblue'
    },
    box_reg:{
        width:300,
        height:100,
        padding:10
    },
    boxStyle_reg:{
        padding:140,
    },
    
    link_reg:{
        color:'blue',
        fontSize:10,
    },
    mainText_reg:{
        color:'#6e8b3d',
        fontSize:20,
        textAlign:"center"
    },
    input_reg:{
        margin:20,
        height:40,
        borderColor:'green',
        borderWidth:1,
        borderRadius:5,
        fontSize:20,
    },
    submitButton_reg:{
        backgroundColor:'#6e8b3d',
        height:30,
        width:80,
        margin:16,
        borderRadius:15,
    },
    submitButtonText_reg:{
        color:'white',
        fontSize:19,
        textAlign:'center'
    }

})

