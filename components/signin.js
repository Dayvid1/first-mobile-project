import React, {Component} from 'react'
import {View, Text, TouchableOpacity, TextInput, StyleSheet} from 'react-native'

class SignIn extends Component{
    state = {
        email:'',
        password:''
    }
    handleEmail = (text)=>{
        this.setState({email:text})
    }
    handlePassword =(text)=>{
        this.setState({password:text})
    }

    login = (email,pass)=>{
        alert("Your Email is "+email+" and Password is "+pass)
    }

    render(){
        return(
            <View style={styles.container}>
                {/* <Text style={styles.link}>Register Your Account?</Text> */}
                <Text style={styles.mainText}> HP CENTER</Text>
                <View style={styles.box}>
                <TextInput style={styles.input}
                    placeholder='Email'
                    placeholderTextColor='#6e8b3d'
                    autoCapitalize='none'
                    onChangeText={this.handleEmail}
                    underlineColorAndroid="transparent"
                />

                <TextInput style={styles.input}
                    placeholder='Password'
                    placeholderTextColor='#6e8b3d'
                    autoCapitalize='none'
                    onChangeText={this.handlePassword}
                    underlineColorAndroid='transparent'
                    secureTextEntry={true}
                />


                </View>
                <View style={styles.boxStyle}>
                <TouchableOpacity 
                    style={styles.submitButton}
                    onPress={()=>this.login(this.state.email,this.state.password)}
                >
                    <Text style={styles.submitButtonText} adjustsFontSizeToFit={true} >LOGIN</Text>
                </TouchableOpacity>

                </View>
            </View>
        )
    }
}

export default SignIn;

const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        height:700,
        alignItems:'center',
        backgroundColor:'powderblue'
    },
    box:{
        width:300,
        height:100,
        padding:10
    },
    link:{
        color:'blue',
        fontSize:13,
    },
    boxStyle:{
        padding:60,
    },
    mainText:{
        color:'#6e8b3d',
        fontSize:24,
        textAlign:"center"
    },
    input:{
        margin:20,
        height:40,
        borderColor:'green',
        borderWidth:1,
        borderRadius:5,
        fontSize:20,
       
    },
    submitButton:{
        backgroundColor:'#6e8b3d',
        height:30,
        width:80,
        margin:16,
        borderRadius:15,
    },
    submitButtonText:{
        color:'white',
        fontSize:19,
        textAlign:'center'
    }
    
})

